#include <iostream>
#include <thread>
#include <optional>

#include <exe/executors/thread_pool.hpp>
#include <exe/futures/make/submit.hpp>
#include <exe/result/make/ok.hpp>

using namespace exe;
using exe::executors::ThreadPool;

using namespace std::chrono_literals;

//////////////////////////////////////////////////////////////////////

// void Coro(ThreadPool& pool) {
//  // suspension point
//  int v = co_await futures::Submit(pool, [] {
//    std::this_thread::sleep_for(3s);
//    return result::Ok(7);
//  });
//  std::cout << "v = " << v std::endl;
//}

//////////////////////////////////////////////////////////////////////

// Awaiter

template <typename T>
class FutureAwaiter {
 public:
  FutureAwaiter(futures::Future<T> f) : future_(std::move(f)) {

  }

  template <typename Coroutine>
  void AwaitSuspend(Coroutine* handle) {
    std::move(future_).Consume([this, handle](Result<T> result) {
      result_.emplace(std::move(result));
      handle->Resume();
    });
  }

  T AwaitResume() {
    return std::move(**result_);
  }

 private:
  std::optional<Result<T>> result_;
  futures::Future<T> future_;
};

//////////////////////////////////////////////////////////////////////

// Hand-crafted stackless coroutine

class CoroStateMachine {
 public:
  CoroStateMachine(ThreadPool& pool) : pool_(pool) {
  }

  void Start() {
    Resume();
  }

  void Resume() {
    if (label_ == 0) {
      auto v_future = futures::Submit(pool_, [] {
        std::this_thread::sleep_for(3s);
        return result::Ok(7);
      });

      label_ = 1;
      // Suspend.1

      v_awaiter_.emplace(std::move(v_future));
      v_awaiter_->AwaitSuspend(this);
      return; // Suspend.2
    } else if (label_ == 1) {
      int v = v_awaiter_->AwaitResume();
      std::cout << "v = " << v << std::endl;
      delete this;
    }
  }

 private:
  // Arguments
  ThreadPool& pool_;

  // Local variables
  std::optional<FutureAwaiter<int>> v_awaiter_;

  int label_ = 0;
};

//////////////////////////////////////////////////////////////////////

int main() {
  ThreadPool pool{4};
  pool.Start();

  auto* coro = new CoroStateMachine{pool};
  coro->Start();

  pool.WaitIdle();
  pool.Stop();

  return 0;
}
