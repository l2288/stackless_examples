#pragma once

#include <await/futures/core/future.hpp>

#include <experimental/coroutine>
#include <optional>

//////////////////////////////////////////////////////////////////////

// Promise Types

namespace await::futures {

template <typename T>
struct CoroutinePromise {
  std::optional<await::futures::Promise<T>> promise_;

  auto get_return_object() {
    auto [f, p] = await::futures::MakeContract<T>();
    promise_.emplace(std::move(p));
    return std::move(f);
  }

  void return_value(T value) {
    std::move(*promise_).SetValue(std::move(value));
  }

  void set_exception(std::exception_ptr e) {
    std::move(*promise_).SetError(std::move(e));
  }

  void unhandled_exception() {
    std::move(*promise_).SetError(std::current_exception());
  }

  std::experimental::suspend_never initial_suspend() noexcept {
    return {};
  }

  std::experimental::suspend_never final_suspend() noexcept {
    return {};
  }
};

}  // namespace await::futures

template <typename R, typename... Args>
struct std::experimental::coroutine_traits<await::futures::Future<R>, Args...> {
  using promise_type = await::futures::CoroutinePromise<R>;
};

//////////////////////////////////////////////////////////////////////

namespace await::futures {

template <>
struct CoroutinePromise<void> {
  std::optional<Promise<void>> promise_;

  auto get_return_object() {
    auto [f, p] = await::futures::MakeContract<void>();
    promise_.emplace(std::move(p));
    return std::move(f);
  }

  std::experimental::suspend_never initial_suspend() noexcept {
    return {};
  }

  std::experimental::suspend_never final_suspend() noexcept {
    return {};
  }

  void set_exception(std::exception_ptr e) {
    std::move(*promise_).SetError(std::move(e));
  }

  void unhandled_exception() {
    std::move(*promise_).SetError(std::current_exception());
  }

  void return_void() {
    std::move(*promise_).Set();
  }
};

}  // namespace await::futures

template <typename... Args>
struct std::experimental::coroutine_traits<await::futures::Future<void>,
                                           Args...> {
  using promise_type = await::futures::CoroutinePromise<void>;
};

//////////////////////////////////////////////////////////////////////

// Awaiter type

namespace await::futures {

template <typename T>
class FutureAwaiter {
 public:
  FutureAwaiter(Future<T>&& f) : future_(std::move(f)) {
  }

  bool await_ready() {
    return false;
  }

  void await_suspend(std::experimental::coroutine_handle<> h) {
    std::move(future_).Subscribe([this, h](wheels::Result<T> result) mutable {
      result_.emplace(std::move(result));
      h.resume();
    });
  }

  auto await_resume() {
    return (*result_).Unwrap();
  }

 private:
  Future<T> future_;
  std::optional<wheels::Result<T>> result_;
};

template <typename T>
auto operator co_await(Future<T>&& f) {
  return FutureAwaiter<T>(std::move(f));
}

}  // namespace await::futures
