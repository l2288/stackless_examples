#include <iostream>
#include <thread>

#include <exe/executors/thread_pool.hpp>
#include <exe/futures/make/submit.hpp>
#include <exe/result/make/ok.hpp>

using namespace exe;
using exe::executors::ThreadPool;

using namespace std::chrono_literals;

//////////////////////////////////////////////////////////////////////

// void Coro(ThreadPool& pool) {
//  int v = co_await futures::Submit(pool, [] {
//    std::this_thread::sleep_for(3s);
//    return result::Ok(7);
//  });
//  std::cout << "v = " << v << std::endl;
//}

//////////////////////////////////////////////////////////////////////

// Hand-crafted stackless coroutine

class CoroStateMachine {
 public:
  CoroStateMachine(ThreadPool& pool) : pool_(pool) {
  }

  void Start() {
    Resume();
  }

  void Resume() {
    if (label_ == 0) {
      auto v_future = futures::Submit(pool_, []() {
        std::this_thread::sleep_for(3s);
        return result::Ok(7);
      });

      label_ = 1;
      // <--

      std::move(v_future).Consume([this](Result<int> result) {
        this->v_ = *result;  // Ignore errors =(
        this->Resume();
      });
      return;  // Suspend
    } else if (label_ == 1) {
      std::cout << "v = " << v_ << std::endl;
      delete this;
    }
  }

 private:
  // Arguments
  ThreadPool& pool_;

  // Local variables
  int v_;

  int label_ = 0;
};

//////////////////////////////////////////////////////////////////////

int main() {
  ThreadPool pool{4};
  pool.Start();

//  {
//    CoroStateMachine coro{pool};
//    coro.Start();
//  }

  auto* coro = new CoroStateMachine{pool};
  coro->Start();

  pool.WaitIdle();
  pool.Stop();

  return 0;
}
